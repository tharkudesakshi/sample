# Print the three most common characters along with their occurrence count.
# Sort in descending order of occurrence count.
# If the occurrence count is the same, sort the characters in alphabetical order.

from collections import Counter

S = input()
S = sorted(S)

abc = Counter(list(S))

for x, y in abc.most_common(3):  #most common is method of counter class
    print(x, y)