# Mr. Vincent works in a door mat manufacturing company. One day, he designed a new door mat with the following specifications:

# Mat size must be X. ( is an odd natural number, and  is  times .)
# The design should have 'WELCOME' written in the center.
# The design pattern should only use |, . and - characters.

n,m = input().split()
n = int(n)
m = int(m)
#printing first half
for i in range(n//2):
    t = int((2*i)+1)
    print(('.|.'*t).center(m, '-'))
#printing middle line
print('WELCOME'.center(m,'-'))
#printing last half
for i in reversed(range(n//2)):
    t = int((2*i)+1)
    print(('.|.'*t).center(m, '-'))